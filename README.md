# Introduction 
This project aims to construct and generate a PDF release note document from the different components of the CHESS application:
- CHESS-Core
- CHESS-Web
- CHESS-API-Portal
- CHESS-X-Engine

# Getting Started

1.	Installation for Windows
To get started with this code, you need to first download pandoc (https://pandoc.org/installing.html) and wkhtmltopdf (https://wkhtmltopdf.org/downloads.html) as they constitute the base of the conversion of the markdown files to PDF. 
Then, you have to setup system variable PATH for pandoc and wkhtmltopdf.
Last, you can use the file requirements.txt to install the packages required with command:

```python
    pip install -r requirements.txt
```

2.	Software dependencies
To execute this code, run the main.py script located in "generator" directory after configuring the different versions of CHESS components using clients/*.config
```bash
python main.py -c client.site.config -o <ORGANISATION> -p <TOKEN>
```
# Build and Test
To run the tests, execute pytest from the base directory using the following code:
```bash
pytest -s -v
```
# Contribute
Feedbacks are welcome. You can use this program as you want :)
If you introduce any changes, please make sure to update tests.