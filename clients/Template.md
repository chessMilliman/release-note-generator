# Release Note 00.00.00
---
### New Features
- <REFERENCE_ID1>: description
- <REFERENCE_ID2>: description


### Bug Fix
- <TICKET_ID1>: description
- <TICKET_ID2>: description


### Important Notes
- Some other deployment notes
- Some other design notes


### Build Validation
	<BUILD_ID>: OK/NOK

