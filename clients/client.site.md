# Azure Specifications
---
### CHESS Web
- Linux (ubuntu 18.04)
- Standard DS3 v2 (4 vcpus, 14 GiB memory)
- Os disk: Premium SSD LRS (64 GiB, 240 Max IOPS, 50 Max throughput MBps and SSE with PMK Encryption)


### CHESS Api-Portal
- Linux (ubuntu 18.04)
- Standard DS3 v2 (4 vcpus, 14 GiB memory)
- Os disk: Premium SSD LRS (30 GiB, 120 Max IOPS, 25 Max throughput MBps and SSE with PMK Encryption)


### CHESS X-Engine
- Virtual machine scale set 
- Standard_B8ms (1-4 scalable instances, 8 vcpus, 32 GiB memory)
- Os disk: Premium SSD LRS (30 GiB, 120 Max IOPS, 25 Max throughput MBps and SSE with PMK Encryption)
- Data disk: Premium SSD LRS (512 GiB, 2300 Max IOPS, 150 Max throughput MBps and SSE with PMK Encryption)