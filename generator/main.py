import argparse
import requests
import json
import base64
import pypandoc

from datetime import date
from http.client import responses

CLIENT_DIR = "../clients"
OUTPUT_DIR = "."
RELEASENOTES_DIR = "/releasenotes"

def get_config(client_conf):
    comp_vers = {}
    aux_list = []
    try:
        with open(f"{CLIENT_DIR}/{client_conf}", 'r') as conf:
            for line in conf:
                line_split = line.rstrip().split(':')
                if len(line_split)>=2:
                    component = line_split[0]
                    version = line_split[1]
                    aux_list.append([component.lower().strip(), version.lower().strip()])
                else:
                    print("Skip line without ':'", line_split)
    except FileNotFoundError as err:
        raise Exception(f"File {client_conf} not found. Aborting")
    except OSError:
        raise Exception(f"OS error occurred trying to open {client_conf}")
    except Exception as exc:
        raise Exception(f"Unexpected error opening {client_conf} is ", repr(exc))

    # Append to list if versions and remove duplicates
    for l in aux_list:
        comp_vers.setdefault(l[0], []).append(l[1])
        comp_vers[l[0]] = list(dict.fromkeys(comp_vers[l[0]]))

    return comp_vers


def get_releasenotes(azure_organization, personal_access_token, project, releasenotes_version):
    project_name = f"CHESS-{project}"
    repository_name = f"CHESS-{project}"
    file_path = f"{RELEASENOTES_DIR}/{releasenotes_version}.md"

    try:
        url = f"https://dev.azure.com/{azure_organization}/{project_name}/_apis/sourceProviders/TfsGit/filecontents?repository={repository_name}&path={file_path}&commitOrBranch=master&api-version=5.1-preview.1"
        combined_pat_token = ":" + personal_access_token
        
        headers = {}
        headers['Content-Type'] = "text/html"
        headers['Authorization'] = b'Basic ' + base64.b64encode(combined_pat_token.encode('utf-8'))
        
        response = requests.get(url, headers=headers)
        response.raise_for_status()
    except requests.exceptions.HTTPError as exc:
        try:
            message = json.loads(response.text)['message']
            raise Exception(f"Http error from {url}:", message)
        except Exception:
            raise Exception("Http error:", repr(exc))
    except requests.exceptions.ConnectionError as exc:
        raise Exception("Error connecting:", repr(exc))
    except requests.exceptions.Timeout as exc:
        raise Exception("Timeout error:", repr(exc))
    except requests.exceptions.RequestException as exc:
        raise Exception("Request error", repr(exc))

    if response.status_code != 200:
        raise Exception(f"Status code {response.status_code}: {responses[response.status_code]} received from {url}")

    return response.text


def get_todays_date():
   return date.today().strftime("%d_%m_%y")


def get_todays_date_altform():
    return date.today().strftime("%Y/%m/%d")


def generate_releasenotes(md_file, name):
    todays_date = get_todays_date()
    try:
        pypandoc.convert_file(md_file,'html5', 'gfm',
                              extra_args=['--metadata=pagetitle:"releaseNotes.md"', '--pdf-engine=wkhtmltopdf', f'--css={OUTPUT_DIR}/github.css'],
                              outputfile=(f"{OUTPUT_DIR}/{name}_ReleaseNote_{todays_date}.pdf"))
    except Exception as exc:
        raise Exception(f"Generating release note from {md_file} failed: ", repr(exc))
    
    print(f"{OUTPUT_DIR}/{name}_ReleaseNote_{todays_date}.pdf was created ")


def main(client_conf, azure_organization):
    print("Called with ", client_conf)
    config_content = get_config(client_conf)
    releasenotes_list = []
    for k,v in config_content.items():
        if k.lower() == "system":
            try:
                with open(f'{CLIENT_DIR}/{v[0]}.md') as system_info:
                    releasenotes_list.append(f'<H1 style="margin-top:2cm;margin-bottom:-0.5cm"><FONT COLOR="#0A4977">System Information</FONT></H1>' + '\n\n' + system_info.read())
            except IOError:
                pass
        else:
            component_release_note = f'<H1 style="margin-top:2.5cm;margin-bottom:-0.5cm"><FONT COLOR="#0A4977">CHESS {"-".join([e.capitalize() for e in k.split("-")])}</FONT></H1>\n\n'
            for ver in v: 
                component_release_note += get_releasenotes(azure_organization, personal_access_token, k, ver) + '\n\n'
            releasenotes_list.append(component_release_note)

    with open(f'{OUTPUT_DIR}/temp-releaseNotes.md', 'w+') as f:
        f.write("""<p align="right" width="100%">
    <img width="33%" src="Milliman logo.png">
</p>""")
        f.write(f"<p align='right' width='100%' style='margin-bottom:-1cm'>Versions of CHESS components released to {client_conf[:-(len('.config'))]}<br> Document generated on {get_todays_date_altform()}</p>\n")
        f.write("\n\n\n\n\n".join(releasenotes_list))

    client_name = client_conf[:-len('.config')]

    generate_releasenotes(f'{OUTPUT_DIR}/temp-releaseNotes.md', client_name)
    

if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser(description='PDF generator from release notes md files.')
    parser.add_argument("-c", "--client_conf", type=str, required=True, help='The client.conf file for selecting release notes versions')
    parser.add_argument("-o", "--organization", type=str, required=True, help='The organization name of Azure DevOps')
    parser.add_argument("-a", "--personal_access_token", type=str, required=True, help='The personal access token to connect to Azure DevOps')
    args = parser.parse_args()
    print("Given args: {}".format(args))
    main(args.client_conf, args.organization, args.personal_access_token)