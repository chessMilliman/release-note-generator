from urllib import request
import pytest
import mock
import requests_mock
import requests
import os

import generator.main as gm

def test_get_config(tmpdir):
    """
    GIVEN a valid conf_file with correct entries inside
    WHEN the get_config function is called
    THEN a dict with the component and their versions must be returned
    """
    tmpath = tmpdir.mkdir("clients")
    with open(f"{tmpath}/test.config", 'w+') as out:
            out.write("Core : 01.00.00\nWeb :01.00.01\nApi-Portal: 01.00.02\nX-Engine:01.00.03")

    with mock.patch("generator.main.CLIENT_DIR", tmpath):
        client_conf = "test.config"
        comp_vers = gm.get_config(client_conf)
        assert(comp_vers == {"core": ["01.00.00"], "web": ["01.00.01"], "api-portal": ["01.00.02"], "x-engine": ["01.00.03"]})


def test_get_config_empty_input(tmpdir):
    """
    GIVEN a valid conf_file but with duplicate entries inside
    WHEN the get_config function is called
    THEN an ConfigFileException exception must be raised to
    """
    tmpath = tmpdir.mkdir("clients")
    with open(f"{tmpath}/test.config", 'w+') as out:
            out.write("")

    with mock.patch("generator.main.CLIENT_DIR", tmpath):
        client_conf = "test.config"
        comp_vers = gm.get_config(client_conf)
        assert(comp_vers == {})


def test_get_config_duplicate_components(tmpdir):
    """
    GIVEN a valid conf_file but with duplicate entries inside
    WHEN the get_config function is called
    THEN an ConfigFileException exception must be raised to
    """
    tmpath = tmpdir.mkdir("clients")
    with open(f"{tmpath}/test.config", 'w+') as out:
            out.write("Core : 01.00.00\nWeb :01.00.01\nApi-Portal: 01.00.02\nX-Engine:01.00.03\nCore: 01.00.04")

    with mock.patch("generator.main.CLIENT_DIR", tmpath):
        client_conf = "test.config"
        comp_vers = gm.get_config(client_conf)
        assert(comp_vers == {"core": ["01.00.00", "01.00.04"], "web": ["01.00.01"], "api-portal": ["01.00.02"], "x-engine": ["01.00.03"]})


def test_get_config_duplicate_components_duplicate_versions(tmpdir):
    """
    GIVEN a valid conf_file but with duplicate entries inside
    WHEN the get_config function is called
    THEN an ConfigFileException exception must be raised to
    """
    tmpath = tmpdir.mkdir("clients")
    with open(f"{tmpath}/test.config", 'w+') as out:
            out.write("Core : 01.00.00\nWeb :01.00.01\nApi-Portal: 01.00.02\nX-Engine:01.00.03\nCore: 01.00.00")

    with mock.patch("generator.main.CLIENT_DIR", tmpath):
        client_conf = "test.config"
        comp_vers = gm.get_config(client_conf)
        assert(comp_vers == {"core": ["01.00.00"], "web": ["01.00.01"], "api-portal": ["01.00.02"], "x-engine": ["01.00.03"]})


def test_get_config_extra_entries(tmpdir):
    """
    GIVEN a valid conf_file with an extra entry inside
    WHEN the get_config function is called
    THEN an ConfigFileException exception must be raised to
    """
    tmpath = tmpdir.mkdir("clients")
    with open(f"{tmpath}/test.config", 'w+') as out:
            out.write("Core : 01.00.00\nWeb :01.00.01\nApi-Portal: 01.00.02\nX-Engine:01.00.03\nSensitivity: 01.00.04")

    with mock.patch("generator.main.CLIENT_DIR", tmpath):
        client_conf = "test.config"
        comp_vers = gm.get_config(client_conf)
        assert(comp_vers == {"core": ["01.00.00"], "web": ["01.00.01"], "api-portal": ["01.00.02"], "x-engine": ["01.00.03"], "sensitivity": ["01.00.04"]})


def test_get_releasenotes_not_found():
    """
    GIVEN a valid component and but non existing version.md in /releasenotes directory
    WHEN the get_releasenotes is called
    THEN a ReleaseNoteException exception must be raised to
    """
    project_name = "CHESS-Core"
    repository_name = "CHESS-Core"
    file_path = f"{gm.releasenotes_repo}/01.00.00.md"
    with requests_mock.Mocker() as m:
        m.get(f"https://dev.azure.com/{gm.azure_organization}/{project_name}/_apis/sourceProviders/{gm.sourceProvider}/filecontents?repository={repository_name}&path={file_path}&commitOrBranch=master&api-version=5.1-preview.1", status_code=404)
        with pytest.raises(gm.ReleaseNoteException):
            gm.get_releasenotes("Core", "01.00.00")


def test_get_releasenotes_invalid_token():
    """
    GIVEN a valid component and but non existing version.md in /releasenotes directory
    WHEN the get_releasenotes is called but an invalid ACCESS_TOKEN
    THEN a ReleaseNoteException exception must be raised to
    """
    project_name = "CHESS-Core"
    repository_name = "CHESS-Core"
    file_path = f"{gm.releasenotes_repo}/01.00.00.md"
    with mock.patch("generator.main.PERSONAL_ACCESS_TOKEN", "123abc"):
        with pytest.raises(gm.ReleaseNoteException):
            gm.get_releasenotes("Core", "01.00.00")


def test_generate_releasenotes(tmpdir):
    """
    GIVEN a valid path to a markdown file
    WHEN the generate_releasenotes is called
    THEN a PDF file must be created
    """
    tmpath = tmpdir.mkdir("test")
    with open(f"{tmpath}/test.md", 'w+') as out:
        out.write("""
# Test

This is some test release notes

## VERSION: 00.00.01

Created a new project and implemented a way to generate a pdf from md files
Make sure to create an config file and put it in /clients directory

## Usage
```bash
python main.py test.config
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
        """)

    with mock.patch("generator.main.OUTPUT_DIR", tmpath):
        filename = 'github.css'
        url = 'https://raw.githubusercontent.com/jgm/pandoc/master/data/templates/styles.html' + filename
        r = requests.get(url)
        open(f"{tmpath}/{filename}" , 'wb').write(r.content)
        with mock.patch("generator.main.get_todays_date", return_value="24_01_2022"):
            gm.generate_releasenotes(f"{tmpath}/test.md", "test")
        
    assert(os.path.exists(f"{tmpath}/test_ReleaseNote_24_01_2022.pdf"))